# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Miscellaneous utilities used in snakemake workflows.
"""
import os
import sys
import string
from glob import glob
from shutil import rmtree
import warnings
from collections.abc import Mapping
from contextlib import contextmanager
from itertools import chain
from subprocess import Popen, PIPE
from re import compile as rcompile, sub
from textwrap import indent
from _string import formatter_field_name_split
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from snakemake import shell
from snakemake.io import apply_wildcards
# To parse genome size information
from bs4 import BeautifulSoup

OPJ = os.path.join


def formatwarning(message, category, filename, lineno, line=None):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


# To be loaded in snakemake as follows:
# from libworkflows import SHELL_FUNCTIONS
# shell.prefix(SHELL_FUNCTIONS)
SHELL_FUNCTIONS = """
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${{PROGNAME}}: ${{1:-"Unknown Error"}}" 1>&2
    exit 1
}}

count_fastq_reads()
{{
    # $1: file in which to write the number of fastq records
    wc -l | {{ read nblines; echo ${{nblines}} / 4 | bc > ${{1}}; }}
}}

sort_by_seq()
{{
    # niceload --mem 10000M fastq-sort -s || error_exit "fastq-sort failed"
    fastq-sort -s || error_exit "fastq-sort failed"
}}

dedup()
{{
    # sort_by_seq | niceload --mem 10000M remove-duplicates-from-sorted-fastq \\
    sort_by_seq | remove-duplicates-from-sorted-fastq \\
        || error_exit "remove_duplicates_from_sorted_fastq failed"
}}

trim_random_nt()
{{
    # $1: nb of bases to trim at 5' end
    # $2: nb of bases to trim at 3' end
    # niceload --noswap -q cutadapt -u ${{1}} -u -${{2}} - 2> /dev/null \\
    cutadapt -u ${{1}} -u -${{2}} - 2> /dev/null \\
        || error_exit "trim_random_nt failed"
}}

compute_size_distribution()
{{
    # $1: file in which to write the size distribution
    awk 'NR%4==2 {{histo[length($0)]++}} END {{for (l in histo) print l"\\t"histo[l]}}' > ${{1}}
}}

samstats2mapped()
{{
    # $1: file containing the output of samtools stats
    # $2: file in which to put the number of mappers
    grep "^SN" ${{1}} | cut -f 2- | grep "^reads mapped:" | cut -f 2 > ${{2}}
}}
"""

STRIP = str.strip
SPLIT = str.split


def strip_split(text):
    """Hopefully faster text.strip().split("\t")."""
    return SPLIT(STRIP(text), "\t")


def first_line(filename):
    """Extract and split the first line of a file *filename*."""
    with open(filename, "r") as filehandle:
        return STRIP(filehandle.readline())


# import inspect
def texscape(text):
    """Escapes underscores to make a latex-compatible text."""
    # https://stackoverflow.com/a/2654130/1878788
    # currframe = inspect.currentframe()
    # callframe = inspect.getouterframes(currframe)
    # print(f"Escaping {text}\n(Called by {callframe})")
    # print(f"Escaping {text}")
    # return sub("_", r"\_", text)
    # To avoid double escape:
    return sub(r"([^\\])_", r"\1\_", text)


def ensure_relative(path, basedir):
    """Returns the relative path to *path* from *basedir*.
    That way, a snakefile can be included using its absolute path."""
    if os.path.isabs(path):
        return os.path.relpath(path, basedir)
    return path


# https://stackoverflow.com/a/15728287/1878788

class PartialFormatter(string.Formatter):
    """Based on https://stackoverflow.com/a/15728287/1878788"""
    def get_field(self, field_name, args, kwargs):
        try:
            val = super(PartialFormatter, self).get_field(
                field_name, args, kwargs)
        except (IndexError, KeyError, AttributeError):
            first, _ = formatter_field_name_split(field_name)
            val = '{' + field_name + '}', first
        return val


partial_format = PartialFormatter().format


def wc_applied(source_function):
    """
    This can be used as a decorator for rule input sourcing functions.

    This ensures that results returned in the form of explicit output from a
    rule has the wildcard substitution applied.

    See <https://bitbucket.org/snakemake/snakemake/issues/956/placeholders-not-properly-matched-with>.
    """
    def wc_applied_source_func(wildcards):
        filename_templates = source_function(wildcards)
        if not isinstance(filename_templates, (str, bytes)):
            return [
                apply_wildcards(filename_template, wildcards)
                for filename_template in filename_templates]
        return apply_wildcards(filename_templates, wildcards)
    return wc_applied_source_func


def run_with_modules(
        shell_commands,
        generic_shell_commands=None, modules=None):
    """
    Return *shell_commands* with extra loading and unloading of *modules*.

    If *generic_shell_commands* is set, a pair of strings will be returned,
    the second using *generic_shell_commands* instead of *shell_commands*.
    *modules* should be a list of environment module names
    (see http://modules.sourceforge.net/).
    """
    if modules is None or not modules:
        # modules = []
        real_cmd = shell_commands
    else:
        module_load = "\n".join([
            f"module load {module};" for module in modules])
        module_unload = "\n".join([
            f"module unload {module};" for module in modules])

        # Prepending "module purge" prevents non-zero exit codes that happen
        # when a module to be loaded is already loaded.
        # Maybe it would be better to just detect and ignore those cases?
        # The `snakemake` variable (present in shell_commands)
        # is not defined here.
        # This has to do with how `format` uses `inspect` in
        # `snakemake/utils.py`
        # We should use shell in the calling context, not right here.
        # shell("\n".join([module_load, shell_commands, module_unload]))
        real_cmd = "\n".join([
            "module purge", module_load, shell_commands, module_unload])
        if generic_shell_commands is not None:
            generic_shell_commands = "\n".join([
                "module purge", module_load,
                generic_shell_commands, module_unload])
    if generic_shell_commands is None:
        return real_cmd
    return (real_cmd, generic_shell_commands)


def envmods(
        mod_name,
        module_versions):
    """
    Generate a list of string representing the modules to be loaded.

    *mod_name* will be used as a key in the *module_versions*
    dictionary in order to get the associated version number.

    If the dictionary entry for *mod_name* is actually a sub-dictionary
    of dependencies for module *mod_name* (including *mod_name* itself),
    the list of modules will be those in this sub-dictionary.
    """
    if isinstance(module_versions[mod_name], Mapping):
        return list(chain.from_iterable(
            envmods(dependency, module_versions[mod_name])
            for dependency in module_versions[mod_name]))
    return [f"{mod_name}/{module_versions[mod_name]}"]


def run_and_write_methods(
        cmd, params, output,
        module_versions=None,
        methods_before="", methods_after="",
        generic_cmd=None,
        verbose=False):
    """
    Run a shell command *cmd*, optionally loading some environment modules.

    If *params* has an attribute "modules" containing a list of environment
    modules, the command loading the modules will be pre-pended to the main
    command *cmd*. A *module_versions* dictionary should then also be provided.

    if *output* has a "methods" attribute, the command *cmd* will be written
    into the corresponding file. *methods_before* and *methods_after* will also
    be written in this file, before and after the command, respectively.

    If *generic_cmd* is set, it will be used in the methods instead of *cmd*.
    """
    if hasattr(params, "modules"):
        # TODO (12/02/2024): Use the native mechanism?
        # https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#using-environment-modules
        # Check how module loading is logged.
        # (29/08/2024) -> not compatible with run directive
        try:
            # Only one module, whose dependencies
            # are assumed to be defined as a sub-dictionary of module_versions
            [mod_name] = params.modules
            modules = envmods(mod_name, module_versions)
        except ValueError:
            modules = list(chain.from_iterable(
                envmods(mod_name, module_versions)
                for mod_name in params.modules))
        cmd = run_with_modules(
            cmd,
            generic_shell_commands=generic_cmd,
            modules=modules)
        if generic_cmd is not None:
            (cmd, generic_cmd) = cmd
    if verbose:
        print(cmd)
    shell(cmd)
    if hasattr(output, "methods"):
        if generic_cmd is None:
            cmd_for_methods = indent(cmd, "\t")
        else:
            cmd_for_methods = indent(generic_cmd, "\t")
        with open(output.methods, "w") as methods_fh:
            methods_fh.write("\n\n".join([
                methods_before,
                cmd_for_methods,
                methods_after + "\n"]))


def cleanup_and_backup(output_dir, config, delete=False):
    """Performs cleanup and backup according to the information present in the
    *config* dictionary."""
    print("removing metadata")
    # https://stackoverflow.com/a/45614282/1878788
    rmtree(".snakemake/metadata")
    if config.get("backup", {}):
        user = config["backup"]["user"]
        if user == "USER":
            user = os.environ[user]
        host = config["backup"]["host"]
        # If no dest_dir, we assume the directory structure
        # is the same on the destination host
        dest_dir = config["backup"].get("dest_dir", os.getcwd())
        # TODO: test this to use lftp to send results to caserta
        # dest_dir = OPB(dest_dir)
        # f"cd ..; lftp cecerelab:elegans2015@157.99.85.153 -e \"cd DataMHE/Analyses/{user}; mirror -RL {dest_dir}/; exit\""
        if delete:
            rsync_options = ("-vaP --exclude=\"*.fastq.gz\" "
                             "--exclude=\".snakemake\" "
                             "--delete")
        else:
            rsync_options = ("-vaP --exclude=\"*.fastq.gz\" "
                             "--exclude=\".snakemake\"")
        # TODO: only upload logs when dest_dir[-4:] == "_err"
        # TODO: use rsync_options
        try:
            print(f"backuping results to {user}@{host}:{dest_dir}")
            shell(f"rsync -vaP {output_dir} {user}@{host}:{dest_dir}")
        # TODO: find the explicit and correct exception
        except Exception:
            print(f"backuping results to {user}@sftpcampus.pasteur.fr:{dest_dir}")
            shell(f"rsync -vaP {output_dir} {user}@sftpcampus.pasteur.fr:{dest_dir}")


def read_int_from_file(filename):
    """Just reads a single integer from a file."""
    return int(first_line(filename))
    # with open(filename, "r") as fh:
    #     return int(fh.readline().strip())


def read_float_from_file(filename):
    """Just reads a single float from a file."""
    return float(first_line(filename))
    # with open(filename, "r") as fh:
    #     return float(fh.readline().strip())


@contextmanager
def warn_context(warn_filename, mode="w"):
    """
    Yield a function that writes warnings in file *warn_filename*
    (and also to stderr).

    *mode* indicates the opening mode for the file. By default it is "w".
    This will overwrite the file if it already exists.
    Set the mode to "a" to append to an existing file.
    """
    with open(warn_filename, mode) as warn_file:
        def warn_to_file(message, category, filename, lineno,
                         file=None, line=None):
            formatted = warnings.formatwarning(
                message, category, filename, lineno, line)
            # echo to stderr
            print(formatted, file=sys.stderr)
            warn_file.write(formatted)
        warnings.showwarning = warn_to_file
        yield warnings.warn


def get_chrom_sizes(filename):
    """Parses an illumina iGenome GenomeSize.xml file.
    Returns a dictionary with chromosome names as keys
    and chromosome lengths as values."""
    with open(filename, "r") as genome_size_file:
        return {
            chrom["contigname"]: int(chrom["totalbases"])
            for chrom in BeautifulSoup(
                genome_size_file, "html5lib").sequencesizes.findAll(
                    "chromosome")}
        # return dict([
        #     (chrom["contigname"], int(chrom["totalbases"]))
        #     for chrom in BeautifulSoup(
        #         genome_size_file, "html5lib").sequencesizes.findAll(
        #             "chromosome")])


# NOTE: It is not clear that this really does what I think it does:
# https://www.biostars.org/p/161534/
# https://www.biostars.org/p/170406/
def feature_orientation2stranded(LIB_TYPE):
    """Create a function to determine stranding options for featureCounts.

    *LIB_TYPE* is a code for the type of library according to
    <https://salmon.readthedocs.io/en/latest/library_type.html#fraglibtype>
    """
    def feature_stranded(wildcards):
        """Determine the correct stranding option for featureCounts."""
        orientation = wildcards.orientation
        if orientation == "fwd":
            if LIB_TYPE[-2:] == "SF":
                return 1
            if LIB_TYPE[-2:] == "SR":
                return 2
            raise ValueError(f"{LIB_TYPE} library type not compatible "
                             "with strand-aware read counting.")
        if orientation == "rev":
            if LIB_TYPE[-2:] == "SF":
                return 2
            if LIB_TYPE[-2:] == "SR":
                return 1
            raise ValueError(f"{LIB_TYPE} library type not compatible "
                             "with strand-aware read counting.")
        if orientation == "all":
            return 0
        exit("Orientation is to be among \"fwd\", \"rev\" and \"all\".")
    return feature_stranded


def sum_htseq_counts(counts_filename):
    """Sum counts in an HTSeq count results file."""
    with open(counts_filename) as counts_file:
        return sum((int(fields[1]) for fields in map(
            strip_split, counts_file) if not fields[0].startswith("__")))


def read_htseq_counts(counts_filename):
    """Extract counts from an HTSeq count results file using pandas."""
    return pd.read_csv(
        counts_filename, sep="\t", header=None, index_col=0).drop(
            ["__no_feature",
             "__ambiguous",
             "__too_low_aQual",
             "__not_aligned",
             "__alignment_not_unique"],
            errors="ignore")


def sum_feature_counts(counts_filename, nb_bams=1):
    """Sum all counts in a featureCounts generated *counts_filename*.

    *nb_bams* indicates the numbre of bam files that were given to
    featureCounts.
    This determines which columns should be used."""
    # Counts are in the 7-th column, starting from third row.
    # The first sum is over the the rows, the second over the columns
    return pd.read_csv(
        counts_filename, sep="\t", skiprows=2,
        usecols=range(6, 6 + nb_bams), header=None).sum().sum()


def read_feature_counts(counts_filename, nb_bams=1):
    """Extract counts from the results of featureCounts."""
    return pd.read_csv(
        counts_filename, sep="\t", skiprows=1,
        usecols=[0, *range(6, 6 + nb_bams)], index_col=0)


# I	3746	3909	"WBGene00023193"	-	.	17996
# I	4118	10230	"WBGene00022277"	-	.	1848
# I	10412	16842	"WBGene00022276"	+	.	4814
# I	17482	26781	"WBGene00022278"	-	.	4282
# I	22881	23600	"WBGene00235381"	-	.	421
# I	27594	32482	"WBGene00022279"	-	.	2237
# I	31522	31543	"WBGene00170953"	+	.	110
# I	32414	32435	"WBGene00173569"	+	.	87
# I	43732	44677	"WBGene00022275"	+	.	24
# I	47471	49819	"WBGene00044345"	+	.	846
def sum_intersect_counts(counts_filename):
    """Sum all counts in a bedtools intersect generated *counts_filename*,
    where the annotation was in bed format.
    """
    # Counts are in the 7-th column
    try:
        return pd.read_csv(
            counts_filename, sep="\t", usecols=[6], header=None).sum().iloc[0]
    except pd.errors.EmptyDataError:
        return "NA"


def read_intersect_counts(counts_filename):
    """Extract counts from results obtained using bedtools intersect."""
    # index_col takes effect after column selection with usecols,
    # hence index_col=0 (ex-third column):
    # https://stackoverflow.com/a/45943627/1878788
    try:
        return pd.read_csv(
            counts_filename, sep="\t",
            usecols=[3, 6], header=None, index_col=0)
    except pd.errors.EmptyDataError:
        return pd.DataFrame(
            index=[], columns=["gene", "counts"]).set_index("gene")


def sum_by_family(counts_data):
    """
    Add a "family" column to *counts_data* and sum the counts for a given
    repeat family.

    The family column is determined assuming that the index contains the repeat
    family name suffixed with a ":" and a number (representing the particular
    instance of the repeat).
    """
    repeat_families = [
        ":".join(name.split(":")[:-1]) for name in counts_data.index]
    return counts_data.assign(family=repeat_families).groupby("family").sum()


# http://stackoverflow.com/a/845069/1878788
def file_len(filename):
    """Get the number of lines of a file."""
    process = Popen(
        ['wc', '-l', filename],
        stdout=PIPE,
        stderr=PIPE)
    result, err = process.communicate()
    if process.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])


def last_lines(filename, nb_lines=1):
    """Extract the last lines of a file."""
    process = Popen(
        ["tail", f"-{nb_lines}", filename],
        stdout=PIPE,
        stderr=PIPE)
    result, err = process.communicate()
    if process.returncode != 0:
        raise IOError(err)
    return result.decode("utf-8")


def test_na_file(filename):
    """Test whether a file is flagged as dummy using "NA"."""
    return first_line(filename) == "NA"
    # with open(fname, "r") as fh:
    #     firstline = fh.readline().strip()
    # return firstline == "NA"


def filter_combinator(combinator, blacklist):
    """This function builds a wildcards combination generator
    based on the generator *combinator* and a set of combinations
    to exclude *blacklist*."""
    def filtered_combinator(*args, **kwargs):
        """This function generates wildcards combinations.
        It is to be used as second argument of *expand*."""
        for wc_comb in combinator(*args, **kwargs):
            # Use frozenset instead of tuple
            # in order to accomodate
            # unpredictable wildcard order
            if frozenset(wc_comb) not in blacklist:
                yield wc_comb
    return filtered_combinator


def column_converter(convert_dict, column_name=None):
    """Generate a function that can be used in *pandas.DataFrame.apply*.

    *convert_dict* will be used to generate new names from those found
    in the index, or in the column given by *column_name*.
    """
    get = convert_dict.get
    if column_name is None:
        def convert_name(row):
            old_name = row.name
            return get(old_name, old_name)
    else:
        def convert_name(row):
            old_name = row[column_name]
            return get(old_name, old_name)
    return convert_name


def plot_text(outfile, text, title=None):
    """
    This can be used to generate dummy figures in case the data is not suitable.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    txt = ax.text(0, 0, text)
    # https://stackoverflow.com/a/21833883/1878788
    txt.set_clip_on(False)
    if title is not None:
        usetex = mpl.rcParams.get("text.usetex", False)
        if usetex:
            title = texscape(title)
        plt.title(title)
    plt.tight_layout()
    plt.savefig(outfile)


def save_plot(outfile,
              plot_func,
              *args,
              title=None, format=None,
              tight=True, equal_axes=False, square=False, rasterize=False,
              **kwargs):
    """*format* is needed when using multiple pages output."""
    # https://stackoverflow.com/a/10154763/1878788
    extra_artists = plot_func(*args, **kwargs)
    if extra_artists is not None:
        save_kwds = {"bbox_extra_artists": extra_artists}
    else:
        save_kwds = dict()
    if title is not None:
        usetex = mpl.rcParams.get("text.usetex", False)
        if usetex:
            title = texscape(title)
        plt.gcf().suptitle(title)
    # Doesn't work?
    # if "x_range" in kwargs:
    #     plt.xlim(kwargs["x_range"])
    # if "y_range" in kwargs:
    #     plt.ylim(kwargs["y_range"])
    if equal_axes:
        # https://stackoverflow.com/a/17996099/1878788
        plt.gca().set_aspect("equal", adjustable="box")
    if square:
        # https://stackoverflow.com/a/18576329/1878788
        plt.gca().set_aspect(1. / plt.gca().get_data_ratio(), adjustable="box")
    if tight:
        save_kwds["bbox_inches"] = "tight"
    if format is None:
        plt.savefig(outfile, **save_kwds)
    else:
        plt.savefig(outfile, format=format, rasterize=rasterize, **save_kwds)
    plt.close(plt.gcf())


def make_id_list_getter(gene_lists_dir, avail_id_lists=None):
    """
    *gene_lists_dir* is the directory in which gene lists are located.
    *avail_id_lists* can be used to restrict the set of files to use.
    If not set, all files ending in "_ids.txt" will be considered.
    """
    if avail_id_lists is None:
        avail_id_lists = set(glob(OPJ(gene_lists_dir, "*_ids.txt")))
    str_attr_err = rcompile("'str' object has no attribute '.*'")

    def get_id_list(wildcards):
        """Instead of a "wildcards" object, a string can be used directly."""
        try:
            id_list = wildcards.id_list
        except AttributeError as err:
            # This may not be a "wildcards" object;
            # Try to use it as a string
            if str_attr_err.match(str(err)) is not None:
                id_list = wildcards
            else:
                raise
        # This may be directly a path
        if id_list in avail_id_lists:
            with open(id_list, "r") as infile:
                return [
                    strip_split(line)[0]
                    for line in infile.readlines() if line[0] != "#"]
        elif id_list == "lfc_statuses":
            return None
        else:
            list_filename = OPJ(gene_lists_dir, f"{id_list}_ids.txt")
            if list_filename in avail_id_lists:
                with open(list_filename, "r") as infile:
                    return [
                        strip_split(line)[0]
                        for line in infile.readlines() if line[0] != "#"]
            else:
                raise NotImplementedError(f"{id_list} unknown.\n")
    return get_id_list
